#!/usr/bin/python
# -*- encoding: utf-8 -*-

import database
import optparse

def event_text(event):
    """From the event info return the text to display it."""
    days = event[1]
    desc = event[0]
    return '%d days till %s' % (days, desc)

def main():
    opt = optparse.OptionParser()
    opt.set_defaults(list=False, random=False)

    opt.add_option('-d', '--database',
            help='Database filename',
            dest='db',
            action='store',
            metavar='FILENAME')

    events = optparse.OptionGroup(opt, 'Events')
    events.add_option('-l', '--list',
            help='List known events',
            dest='list',
            action='store_true')
    events.add_option('-r', '--random',
            help='Print a random event',
            dest='random',
            action='store_true')
    events.add_option('-a', '--add',
            help='Add a new event in the database',
            dest='add',
            action='store',
            type='string',
            nargs=3,
            metavar="DATE DESCRIPTION RECURSIVE")

    opt.add_option_group(events)
    (options, args) = opt.parse_args()
    if not options.db:
        opt.error('A database filename is required')
        return

    db = database.Database(options.db)

    if options.add:
        db.add(*options.add)

    if options.random:
        event = db.random()
        print event_text(event)

    if options.list:
        for event in db.list():
            print event_text(event)

    return

if __name__ == '__main__':
    main()
