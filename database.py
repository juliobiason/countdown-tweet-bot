#!/usr/bin/python
# -*- encoding: utf-8 -*-

import sqlite3
import os
import random
import datetime

def _days(date, recurring):
    """Return the number of days till the event or None in case the date is
    not valid or expired."""
    date = datetime.date(*([int(x) for x in date.split('-')]))
    today = datetime.date.today()

    if date < today:
        if not recurring:
            return None
        date =  date.replace(year=date.year+1)

    diff = date - today
    return diff.days

class Database(object):
    def __init__(self, db_name=None):
        if not db_name:
            db_name = 'events.db'
        self.db_name = db_name

        if not os.access(self.db_name, os.F_OK):
            self.create()

        self.db = sqlite3.connect(self.db_name)
        return

    def create(self):
        """Creates the database file and adds our tables."""
        conn = sqlite3.connect(self.db_name)
        cursor = conn.cursor()

        cursor.execute('''create table events
            (id integer primary key,
            date text,
            name text,
            recurring integer)''')
        conn.commit()
        cursor.close()
        conn.close()
        return

    def add(self, date, description, recurring):
        """Add a new event."""
        # do this to check if it's a valid date.
        date = datetime.date(*([int(x) for x in date.split('-')]))
        date_str = date.strftime('%Y-%m-%d')

        cursor = self.db.cursor()
        cursor.execute('''insert into events
            (id, date, name, recurring)
            values
            (null, ?, ?, ?)''',
            (date_str , description, recurring))
        self.db.commit()
        cursor.close()
        return

    def random(self):
        """Choose a random event. Return a tuple with the name of the event
        and the days till the event."""
        cursor = self.db.cursor()

        cursor.execute('select max(id) from events')
        max = cursor.fetchone()[0]

        random.seed()
        event = None
        while not event:
            choice = random.randint(1, max)
            cursor.execute('''select date, name, recurring from events where
                id = ?''', (choice,))
            row = cursor.fetchone()
            if not row:
                # get another one
                continue

            # convert the date to a datetime object
            days = _days(row[0], row[2])
            name = row[1]

            if not days:
                # event is gone, delete it and try to find another one
                cursor.execute('delete from events where id = ?', (choice,))
                continue

            # and now we have a pure random event! ;)
            event = (name, days)
            break

        self.db.commit()
        cursor.close()
        return event

    def list(self):
        """Return a list with the events. The list will contain tuples with
        the name and the days till the event."""
        cursor = self.db.cursor()

        today = datetime.date.today()
        deletable = []

        cursor.execute('''select id, name, date, recurring from events
                order by date''')
        for row in cursor:
            days = _days(row[2], row[3])
            if not days:
                deletable.append(row[0])
                continue

            yield (row[1], days)

        for id in deletable:
            cursor.execute('''delete from events where id=?''', (id,))

        self.db.commit()
        cursor.close()
        self.db.close()
        return
